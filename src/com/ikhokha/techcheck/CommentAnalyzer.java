package com.ikhokha.techcheck;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class CommentAnalyzer {

	private File file;

	public CommentAnalyzer(File file) {
		this.file = file;
	}

	public Callable<Map<String, Integer> > analyze = () ->{

		Map<String, Integer> resultsMap = new HashMap<>();

		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

			List<String> lines = reader.lines().collect(Collectors.toList());

			long movers = lines.stream().filter(s -> s.toLowerCase().contains("mover")).count();

			incOccurrence(resultsMap, "MOVER_MENTIONS", movers);
			long shakers = lines.stream().filter(s -> s.toLowerCase().contains("shaker")).count();

			incOccurrence(resultsMap, "SHAKER_MENTIONS", shakers);
			long shortComments = lines.stream().filter(s -> s.length() < 15).count();
			incOccurrence(resultsMap, "SHORTER_THAN_15", shortComments);
			long questions = lines.stream().filter(s -> s.contains("?")).count();
			incOccurrence(resultsMap, "QUESTIONS", questions);


			Pattern rgx = Pattern.compile("https?://(www\\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_+.~#?&/=]*)", Pattern.CASE_INSENSITIVE);
			long spam = lines.stream().filter(s -> rgx.matcher(s).find()).count();
			incOccurrence(resultsMap, "SPAM", spam);

		} catch (FileNotFoundException e) {
			System.out.println("File not found: " + file.getAbsolutePath());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IO Error processing file: " + file.getAbsolutePath());
			e.printStackTrace();
		}

		return resultsMap;

	};

	/**
	 * This method increments a counter by 1 for a match type on the countMap. Uninitialized keys will be set to 1
	 * @param countMap the map that keeps track of counts
	 * @param key the key for the value to increment
	 */
	private void incOccurrence(Map<String, Integer> countMap, String key, long value) {

		countMap.putIfAbsent(key, 0);
		countMap.put(key, (int) (countMap.get(key) + value));
	}

}
