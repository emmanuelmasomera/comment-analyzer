package com.ikhokha.techcheck;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
public class Main {

	public static void main(String[] args) throws Exception {
				
		File docPath = new File("docs");
		File[] commentFiles = docPath.listFiles((d, n) -> n.endsWith(".txt"));

		int nThread = commentFiles.length < 10 ? commentFiles.length : commentFiles.length / 4;
		ExecutorService executor = Executors.newFixedThreadPool(nThread);
		Stack<Future<Map<String, Integer>>> futures = new Stack<>();
		Map<String, Integer> totalResults = new HashMap<>();
		
		for (File commentFile : commentFiles) {
			CommentAnalyzer commentAnalyzer = new CommentAnalyzer(commentFile);
			futures.push(executor.submit(commentAnalyzer.analyze));
		}

		while (!futures.empty()) {
			Future<Map<String, Integer>> result = futures.pop();
			Map<String, Integer> items = result.get();

			addReportResults(items, totalResults);
		}

		executor.shutdown();
		
		System.out.println("RESULTS\n=======");
		totalResults.forEach((k,v) -> System.out.println(k + " : " + v));
	}
	
	/**
	 * This method adds the result counts from a source map to the target map 
	 * @param source the source map
	 * @param target the target map
	 */
	private static void addReportResults(Map<String, Integer> source, Map<String, Integer> target) {

		for (Map.Entry<String, Integer> entry : source.entrySet()) {
			Integer data = target.get(entry.getKey());
			target.put(entry.getKey(), data== null?entry.getValue(): entry.getValue() + data);
		}
		
	}

}
